# StarWorld WebServer Plus
> 基于 [StarWorld WebServer](https://github.com/WowStarWorld/StarWorldWebServer) 构建

## 介绍
> 用“Node.js”编写的简单静态服务器 ——原作者注

StarWorld WebServer Plus（以下简称SWS+）是基于 [StarWorld WebServer](https://github.com/WowStarWorld/StarWorldWebServer) 编写的一个轻便的 Node.js 静态网络服务器。
此程序在原程序基础上增加了`view`等指令并大幅优化了软件UI。

## 依赖
1. Node.js

## 构建步骤
1、 克隆本仓库

2、 安装`Node.js`，Linux可使用以下指令安装：

```bash
# Debian / Ubuntu
sudo apt install nodejs

# Arch / Manjaro
sudo pacman -S nodejs

# Red Hat / CentOS
sudo yum install nodejs
```
3、 `node ./main.js`运行

## 使用帮助
### 配置文件
SWS+ 的所有配置均储存在`config.json`，您可以在程序任然可读取的情况下随意修改它

### 指令
SWS+ 内置了几个指令，您可以输入`help`查看他们

### 服务器
程序启动后默认会将服务器启动到`8080`端口，您可以通过`localhost:8080`访问它

服务器文件全部储存在`/server`文件夹下，您可以修改他，不过此程序并不支持PHP

## 捐赠
创作不易，请支持我！
- [爱发电](https://afdian.net/@XiaoDeng3386)